import 'package:bmi_calculator/constant.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator/reusable_card.dart';


class ResultPage extends StatelessWidget {

  ResultPage({@required this.resultText, @required this.bmiResult, @required this.interpretation});

  final String bmiResult;
  final String resultText;
  final String interpretation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI Calculator'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ReusableCard(
              color: activeColor,
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(resultText.toUpperCase(), style: resultTextStyle,),
                  Text(bmiResult, style: bmiTextStyle,),
                  Text(interpretation, style: bodyTextStyle, textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            child: Container(
              alignment: Alignment.center,
              child: Text('RE-CALCULATE', style: largeButtonTextStyle),
              color: Color(0XFFFF1493),
              margin: EdgeInsets.only(top: 10.0),
              width: double.infinity,
              height: buttomContainerHeight,
            ),
            onTap: (){
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}

