import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const buttomContainerHeight = 80.0;
class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI Calculator'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
              child: Row(
            children: <Widget>[
              Expanded(
                child: ReusableCard(color: Color(0XFF1D1E33),
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(FontAwesomeIcons.mars,
                        size: 80.0,
                        color: Color(0XFFFFFFFF),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Text('MALE', style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0XFFFFFFFF),
                      ),),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: ReusableCard(color: Color(0XFF1D1E33),
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(FontAwesomeIcons.mars,
                        size: 80.0,
                        color: Color(0XFFFFFFFF),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text('MALE', style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0XFFFFFFFF),
                      ),),
                    ],
                  ),
                ),
              ),
            ],
          )),
          Expanded(
            child: ReusableCard(color: Color(0XFF1D1E33),
            ),
          ),
          Expanded(
              child: Row(
            children: <Widget>[
              Expanded(
                child: ReusableCard(
                  color: Color(0XFF1D1E33),
                ),
              ),
              Expanded(
                child: ReusableCard(
                color: Color(0XFF1D1E33),),
              ),
            ],
          )),
          Container(
            color: Color(0XFFFF1493),
            margin: EdgeInsets.only(top: 10.0),
            width: double.infinity,
            height: buttomContainerHeight,
          )
        ],
      ),
    );
  }
}

class ReusableCard extends StatelessWidget {
  ReusableCard({@required this.color, this.cardChild});
  final Color color;
  final Widget cardChild;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: cardChild,
      margin: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: color,
    ));
  }
}
