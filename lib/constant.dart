import 'package:flutter/material.dart';

const buttomContainerHeight = 80.0;
const inactiveColor = Color(0XFF1D1E33);
const activeColor = Color(0XFF111328);
const iconColor = Color(0XFFFFFFFF);

const labelTextStyle = TextStyle(
  fontSize:  18.0,
  color: Color(0XFFFFFFFF),
);

const numberTextStyle = TextStyle(
  fontSize:  50.0,
  fontWeight: FontWeight.w900,
  color: Color(0XFFFFFFFF),
);

const largeButtonTextStyle = TextStyle(
  fontSize:  25.0,
  fontWeight: FontWeight.w900,
  color: Color(0XFFFFFFFF),
);

const resultTextStyle = TextStyle(
  fontSize:  20.0,
  fontWeight: FontWeight.bold,
  color: Color(0XFF24D876),
);

const bmiTextStyle = TextStyle(
  fontSize:  100.0,
  fontWeight: FontWeight.bold,
);

const bodyTextStyle = TextStyle(
  fontSize:  22.0,
);